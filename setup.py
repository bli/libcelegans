# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages
from itertools import chain, combinations

name = "libcelegans"
__version__ = "0.2"

id_names = ["cosmid", "wormid", "name"]
script_names = list(chain.from_iterable(
    ["%s2%s" % (name1, name2), "%s2%s" % (name2, name1)]
    for (name1, name2) in combinations(id_names, 2)))

setup(
    name=name,
    version=__version__,
    description="Bioinformatics library providing utilities to deal with C. elegans.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    python_requires=">=3.4, <4",
    packages=find_packages(),
    entry_points={
        "console_scripts": ["%s = idconvert:main" % script_name for script_name in script_names]
    },
    package_data={"idconvert": ["data/geneIDs/*.pickle", "data/geneIDs/cosmid_ID_name.txt"]},
    scripts=["bin/make_gene_id_conversion_dicts.py"],
    #scripts=["bin/%s" % name],
    install_requires=[])
