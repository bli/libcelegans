#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Utilities to convert fields using a pickled dictionary.

When called as a script, parse tab-separated input and converts the fields
using the appropriate pre-installed pickled dictionary.
Input is taken in the file given as argument or in stdin.
"""

import os
from sys import argv, stdin, stdout
# from sys import argv, stderr, stdin, stdout
# To store dictionaries
# from cPickle import load
from pickle import load
from contextlib import contextmanager
from inspect import currentframe, getframeinfo
from pathlib import Path


@contextmanager
def input_stream(filename=None):
    """Yield a file handle for path *filename* or stdin."""
    if filename is None or filename == "-":
        yield stdin
    else:
        with open(filename) as file_h:
            yield file_h


def make_field_converter(pickle_filename, sep="\t"):
    """Create a field converter function based on *pickle_filename*.

    The returned function converts fields from a tab-separated
    text stream using a dictionary pickled at path *pickle_filename*.
    To use something else than tab as field separator, set the *sep*
    argument. This applies to both input and output.
    """
    with open(pickle_filename, "rb") as pickle_file:
        converter = load(pickle_file).get

    def field_converter(input_source):
        """Convert fields from the tab-separated text stream *input_source*.

        If the field is not known by the converter, it is not converted.
        """
        for line in input_source:
            # Use the field as default value
            yield sep.join(
                converter(field, field) for field in line.strip().split(sep))
    return field_converter


def convert_fields(pickle_filename,
                   in_stream=stdin, out_stream=stdout,
                   sep="\t"):
    """Print a modified version of *in_stream* into *out_stream*.

    Fields in tab-separated columns are replaced by the associated value
    in the dictionary pickled in *pickle_filename*.
    To use something else than tab as field separator, set the *sep* argument.
    This applies to both input and output.
    """
    field_converter = make_field_converter(pickle_filename, sep)
    print(*(line for line in field_converter(in_stream)),
          sep="\n", file=out_stream)


def main():
    """Run as a command-line script."""
    # https://stackoverflow.com/a/44188489/1878788
    this_file = Path(getframeinfo(currentframe()).filename).resolve()
    pickle_dir = this_file.parent.joinpath("data", "geneIDs")
    # pickle_dir = os.path.join(
    #     os.path.dirname(os.path.abspath(__file__)), "..", "data", "geneIDs")
    # The name under which the program is called (via a symbolic link)
    # determined the name of the pickle file to use.
    conv_name, _ = os.path.splitext(os.path.basename(argv[0]))
    pickle_filename = os.path.join(pickle_dir, f"{conv_name}.pickle")
    # stderr.write("Using %s\n" % pickle_filename)
    try:
        filename = argv[1]
    except IndexError:
        filename = None
    with input_stream(filename) as in_stream:
        convert_fields(pickle_filename, in_stream)
    return 0


if __name__ == "__main__":
    exit(main())
