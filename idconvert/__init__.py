__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
import os
# The working directory is that of the importing context.
# __path__ gives access to the package path
gene_ids_data_dir = os.path.join(__path__[0], "data", "geneIDs")
from .idconvert import (
    convert_fields,
    input_stream,
    main
    )
