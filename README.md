# Utilities to deal with C. elegans bioinformatics

Currently, this package only provides scripts to convert gene identifiers in
tab-separated columns.


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/libcelegans.git`, `cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/libcelegans.git


## <a name="updating-data"></a>Updating conversion data


The conversion rely on data obtained from Wormbase.
This data may be updated.


### Get the source of identifier correspondence from wormbase

This can be done from the source folder:

    #version="WS253"
    #version="WS260"
    # Setting version="current" will likely get the most current version of the gene id data
    version="current"
    mkdir -p idconvert/data/geneIDs
    cd idconvert/data/geneIDs
    wget ftp://ftp.wormbase.org/pub/wormbase/species/c_elegans/annotation/geneIDs/c_elegans.PRJNA13758.${version}.geneIDs.txt.gz
    gunzip c_elegans.PRJNA13758.${version}.geneIDs.txt.gz
    # Should this be concatenated to the content of `geneIDs.txt`?
    # wget ftp://ftp.wormbase.org/pub/wormbase/species/c_elegans/annotation/geneOtherIDs/c_elegans.PRJNA13758.${version}.geneOtherIDs.txt.gz
    # gunzip c_elegans.PRJNA13758.${version}.geneOtherIDs.txt.gz


### Regenerate the conversion dictionaries

    awk -F "," '{print $4"\t"$2"\t"$3}' c_elegans.PRJNA13758.${version}.geneIDs.txt | sed 's/^\t/\.\t/' | sed 's/\t$/\t\./' | uniq > cosmid_ID_name.txt
    ../../../bin/make_gene_id_conversion_dicts.py cosmid_ID_name.txt


### Re-install the conversion scripts

    # From the top-level of the source folder:
    python3 -m pip install .


## Using identifier conversion scripts

The installation with `pip` should install six command-line conversion scripts,
whose names are in the form `<in_id>2<out_id>`:
* `cosmid2wormid`
* `cosmid2name`
* `wormid2name`
* `wormid2cosmid`
* `name2wormid`
* `name2cosmid`

They can be called in two ways:
* With a file as argument:
    `cosmid2wormid idconvert/data/geneIDs/cosmid_ID_name.txt`
* Taking text from its standard input.
    `cat idconvert/data/geneIDs/cosmid_ID_name.txt | cosmid2wormid`

The text will be parsed as tab-separated data, line-by-line.

On a given line, fields present in the conversion data among identifiers of
type `<in_id>` will be substituted with the corresponding `<out_id>`
identifier.

The result will be printed to standard output.

This only works when the identifier constitutes the whole of the field, and
will have no effect if the identifier is not present in the conversion data.


## Converting from within Python

You can also use the `convert_fields` function inside Python code:

```
from idconvert import convert_fields
with open("idconvert/data/geneIDs/cosmid_ID_name.txt") as file_h:
    convert_fields("idconvert/data/geneIDs/cosmid2wormid.pickle", file_h)
```

In the above example, the conversion is based on information taken
from the Python dictionary saved in
`idconvert/data/geneIDs/cosmid2wormid.pickle`

Such dictionaries can be generated using the procedure described in
the section about [updating conversion data](#updating-data).


## Citing

If you use this package, please cite the following paper:

> Barucci et al, 2020 (doi: [10.1038/s41556-020-0462-7](https://doi.org/10.1038/s41556-020-0462-7))
